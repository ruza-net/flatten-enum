#[macro_export]
macro_rules! generate_flatten {
    ( $variant_path:path ) => {
        pub fn flatten(self) -> Self {
            match self {
                $variant_path (v) => {
                    let mut acc = Vec::new();
                    let mut stack = vec![v];

                    while let Some(curr) = stack.pop() {
                        for x in curr {
                            match x {
                                $variant_path (v) => stack.push(v),

                                s => acc.push(s),
                            }
                        }
                    }

                    $variant_path (acc)
                },

                s => s
            }
        }
    };
}
